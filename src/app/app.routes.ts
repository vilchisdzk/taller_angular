import { RouterModule, Routes } from '@angular/router';
import { PeliculasComponent } from './components/peliculas/peliculas.component';
import { AboutComponent } from './components/about/about.component';
import { InfoPeliculaComponent } from './components/info-pelicula/info-pelicula.component';

const APP_ROUTES: Routes = [
  { path: '', component: AboutComponent },
  { path: 'peliculas', component: PeliculasComponent },
  { path: 'buscar/:str', component: PeliculasComponent },
  { path: 'infoPelicula/:id', component: InfoPeliculaComponent },
  { path: 'about', component: AboutComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
