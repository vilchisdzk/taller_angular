import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  // forma automatica de cargar servicios
  providedIn: 'root'
})

export class PeliculaService {

  private _url: string;
  private _key: string;

  constructor(private _http: HttpClient) {
    this._key = 'your_api_key';
    this._url = 'http://www.omdbapi.com/?apikey=' + this._key + '&';
    console.log('Servicio cargado ' + this._url);
  }

  buscar(query: string, page = 1) {
    return this._http.get(this._url + 's=' + query + '&page=' +  page);
  }

  info(imdbID: string) {
    return this._http.get(this._url + 'i=' + imdbID + '&plot=full');
  }
}
