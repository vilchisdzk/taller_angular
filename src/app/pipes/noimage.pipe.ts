import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimage'
})
export class NoimagePipe implements PipeTransform {

  transform(images: string): string {
    let img = 'assets/img/noimage.png';
    if (images && images !== 'N/A') {
      img = images;
    }
    return img;
  }

}
