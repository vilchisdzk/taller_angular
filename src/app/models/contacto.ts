export class Contacto {
  nombre: String;
  correo: String;
  comentario: String;

  constructor(nombre, correo, comentario) {
    this.nombre = nombre;
    this.correo = correo;
    this.comentario = comentario;
  }
}
