import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AboutComponent } from './components/about/about.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { PeliculasComponent } from './components/peliculas/peliculas.component';

// Pipes
import { NoimagePipe } from './pipes/noimage.pipe';

// Rutas
import { APP_ROUTING } from './app.routes';
import { InfoPeliculaComponent } from './components/info-pelicula/info-pelicula.component';


@NgModule({
  declarations: [
    NoimagePipe,
    AppComponent,
    NavbarComponent,
    AboutComponent,
    FormularioComponent,
    PeliculasComponent,
    InfoPeliculaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
