import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PeliculaService } from './../../services/peliculas.services';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html'
})
export class PeliculasComponent implements OnInit {

  peliculas: any;
  busqueda: string;
  pages: number;
  page: number;
  rowXpages: number;


  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _peliculaService: PeliculaService) {
    this.peliculas = { Search: [], Response: 'False', totalResults: 0};
    this.busqueda = 'LEGO';
    this.pages = 0;
    this.page = 1;
    this.rowXpages = 10;
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params => {
        if (params['str']) {
          this.busqueda = params['str'];
        }
        this.buscarPelicula(this.busqueda, this.page);
      }
    );
  }

  buscarPelicula(search, page) {
    if (page !== this.page) {
      this.page = page;
    }
    this._peliculaService.buscar(this.busqueda, this.page)
      .subscribe((data: any) => {
        this.peliculas = data;
        if (data.Response === 'True') {
          this.pages = Math.ceil(data.totalResults / this.rowXpages);
        }
      }
    );
  }

  verPelicula(id: string) {
    this._router.navigate(['/infoPelicula', id]);
  }

}
