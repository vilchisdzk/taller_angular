import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {}

  buscar(termino: string) {
    this._router.navigate(['/buscar', termino.trim().toUpperCase()]);
  }
}
