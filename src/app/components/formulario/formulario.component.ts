import { Component, OnInit } from '@angular/core';
import { Contacto } from './../../models/contacto';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styles: []
})

export class FormularioComponent implements OnInit {
  public contacto: Contacto;
  public ArrContactos: Contacto[];

  constructor() {
    this.contacto = new Contacto('', '', '');
    this.ArrContactos = [];
  }

  ngOnInit() {
    console.log(this.contacto);
  }

  enviarForm() {
    console.log(this.contacto);
    if (this.contacto.correo !== '') {
      let con = new Contacto(this.contacto.nombre, this.contacto.correo, this.contacto.comentario);
      this.ArrContactos.push(con);
      con = null;
    }
    console.log(this.ArrContactos);
  }

}
