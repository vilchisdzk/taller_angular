import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styles: []
})
export class AboutComponent implements OnInit {
  public alerta: String;
  public tamanio: number;

  constructor() {
    this.alerta = 'info';
    this.tamanio = 20;
  }

  ngOnInit() {}

}
