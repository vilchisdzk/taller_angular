import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { PeliculaService } from './../../services/peliculas.services';

@Component({
  selector: 'app-info-pelicula',
  templateUrl: './info-pelicula.component.html',
  styles: []
})
export class InfoPeliculaComponent implements OnInit {

  pelicula: any[];

  constructor(private _router: Router,
              private _location: Location,
              private _activatedRoute: ActivatedRoute,
              private _peliculaService: PeliculaService) {
      this.pelicula = [];
    }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params => {
        if (params['id']) {
          this._peliculaService.info(params['id'])
            .subscribe((data: any) => {
              this.pelicula = data;
              console.log(this.pelicula);
            }
          );
        }
      }
    );
  }

  goBack() {
    this._location.back();
  }
}
